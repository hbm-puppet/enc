<?xml version="1.0"?>
<project name="Puppet ENC">

  <taskdef resource="net/sf/antcontrib/antlib.xml"/>

  <!-- load properties -->
  <property environment="env"/>
  <property file="build.properties"/>

  <!-- generate name for working directory -->
  <tstamp>
    <format property="timestamp" pattern="yyyyMMddhhmmss"/>
  </tstamp>
  <property name="work" value="temp_${timestamp}"/>

  <target name="deploy">

    <!-- fail if we do not have a value for servers -->
    <if>
      <not><isset property="servers"/></not>
      <then>
        <fail message="Must set -Dservers=&lt;colon-separated list of puppet server fqdns&gt;"/>
      </then>
    </if>

    <antcall target="prepare-working-directory"/>
    <antcall target="prepare-enc-data"/>

    <for list="${servers}" param="puppetserver" delimiter=":">
      <sequential>
        <antcall target="deploy-puppetserver">
          <param name="puppetserver" value="@{puppetserver}"/>
        </antcall>
      </sequential>
    </for>

    <antcall target="delete-working-directory"/>

  </target>

  <target name="prepare-enc-data">

    <!-- get a list of JSON files in the services directory -->
    <pathconvert property="services" pathsep=",">
      <path>
        <fileset dir="services">
          <include name="*.json"/>
        </fileset>
      </path>
      <chainedmapper>
        <flattenmapper/>
        <globmapper from="*" to="services/*"/>
      </chainedmapper>
    </pathconvert>

    <for list="${services}" param="service">
      <sequential>

        <echo message="*** building ENC data for @{service}"/>

        <!-- load the content of the JSON file into a property -->
        <local name="json"/>
        <loadfile property="json" srcFile="@{service}"/>

        <get_nodes from="${json}" into="nodes"/>

        <for list="${nodes}" param="node">
          <sequential>

            <echo message="***   - @{node}"/>

            <!-- get the node hostname and domain -->
            <local name="hostname"/>
            <local name="domain"/>
            <propertyregex property="hostname" input="@{node}" regexp="\..*$" replace="" defaultValue="@{node}"/>
            <propertyregex property="domain" input="@{node}" regexp="^.*?\." replace="" defaultValue=""/>

            <local name="encfile"/>
            <property name="encfile" value="${work}/${domain}/${hostname}.yaml"/>

            <concat destfile="${encfile}" eol="lf">---${line.separator}</concat>

            <local name="environment"/>
            <get_environment for="@{node}" from="${json}" into="environment"/>
            <concat destfile="${encfile}" eol="lf" append="true">environment: ${environment}${line.separator}</concat>

            <set_parameters for="@{node}" from="${json}"/>
            <propertyselector property="nodeproperties" match="node\.@{node}\.(.*)" select="\1"/>
            <if>
              <isset property="nodeproperties"/>
              <then>
                <for list="${nodeproperties}" param="name">
                  <sequential>
                    <local name="value"/>
                    <propertycopy name="value" from="node.@{node}.@{name}"/>
                    <concat destfile="${encfile}" eol="lf" fixlastline="yes" append="true">@{name}: ${value}${line.separator}</concat>
                  </sequential>
                </for>
              </then>
            </if>

            </sequential>
          </for>

      </sequential>
    </for>

  </target>

  <target name="deploy-puppetserver">

    <echo message="*** rsyncing ENC data to ${puppetserver}"/>

    <!-- rsync the environment -->
    <exec executable="rsync">
      <arg line="${options.rsync} ${options.rsync.world}"/>
      <arg value="-e"/>
      <arg value="/usr/bin/ssh ${options.ssh} -i ${env.DEPLOY_KEY}"/>
      <arg value="${work}/"/>
      <arg value="deploy@${puppetserver}:${dir.enc}/"/>
    </exec>

    <!-- fix selinux -->
    <exec executable="ssh">
      <arg line="${options.ssh} -i ${env.DEPLOY_KEY}"/>
      <arg line="deploy@${puppetserver}"/>
      <arg line="/usr/bin/chcon -R -u system_u ${dir.enc}"/>
    </exec>

  </target>

  <target name="clean"/>

  <target name="test">

    <antcall target="prepare-working-directory"/>
    <antcall target="prepare-enc-data"/>

  </target>

  <target name="prepare-working-directory">

    <mkdir dir="${work}"/>

  </target>

  <target name="delete-working-directory">

    <if>
      <not><isset property="debug"/></not>
      <then>
        <delete dir="${work}" deleteonexit="true"/>
      </then>
    </if>

  </target>

  <scriptdef language="javascript" name="get_nodes">
    <attribute name="from"/>
    <attribute name="into"/>
    <![CDATA[

    var from = attributes.get("from");
    var into = attributes.get("into");

    var service = JSON.parse(from);

    var nodes = [];

    for (var tiername in service.tiers) {
      var tier = service.tiers[tiername];
      for (var nodename in tier.nodes) {
        nodes.push(nodename);
      }
    }

    project.setNewProperty(into, nodes.join(','));

    ]]>
  </scriptdef>

  <scriptdef language="javascript" name="get_environment">
    <attribute name="for"/>
    <attribute name="from"/>
    <attribute name="into"/>
    <![CDATA[

    var fornode = attributes.get("for");
    var from = attributes.get("from");
    var into = attributes.get("into");

    var service = JSON.parse(from);

    var environment = service.environment;

    for (var tiername in service.tiers) {
      var tier = service.tiers[tiername];
      for (var nodename in tier.nodes) {
        var node = tier.nodes[nodename];
        if (fornode == nodename) {
          if (tier.environment != null) {
            environment = tier.environment;
          }
          if (node.environment != null) {
            environment = node.environment;
          }
          break;
        }
      }
    }

    project.setNewProperty(into, environment);

    ]]>
  </scriptdef>

  <scriptdef language="javascript" name="set_parameters">
    <attribute name="for"/>
    <attribute name="from"/>
    <![CDATA[

    var fornode = attributes.get("for");
    var from = attributes.get("from");

    var service = JSON.parse(from);

    var tierparams = {};
    var nodeparams = {};

    for (var tiername in service.tiers) {
      var thistier = service.tiers[tiername];
      for (var nodename in thistier.nodes) {
        if (fornode == nodename) {
          var thisnode = thistier.nodes[nodename];
          nodeparams = thisnode.parameters;
          tierparams = thistier.parameters;
          if ( tierparams == null ) {
            tierparams = {};
          }
          tierparams["tier"] = tiername;
          break;
        }
      }
    }

    var parameters = {};
    for (var key in service.parameters) {
      parameters[key] = service.parameters[key];
    }
    for (var key in tierparams) {
      parameters[key] = tierparams[key];
    }
    for (var key in nodeparams) {
      parameters[key] = nodeparams[key];
    }

    for (var key in parameters ) {
      project.setNewProperty('node.' + fornode + '.' + key, parameters[key]);
    }

    ]]>
  </scriptdef>


</project>
