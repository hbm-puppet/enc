node {
  try {
    withCredentials([
      file(credentialsId: '31c5a7aa-ea59-4221-a810-5d92ad299f8c', variable: 'DEPLOY_KEY')
    ]) {
      notifyHipchat("STARTED")
      checkout scm
      sh "ant deploy -Dservers=${PUPPET_SERVERS}"
    }
  } catch (e) {
    currentBuild.result = "FAILED"
  } finally {
    notifyHipchat(currentBuild.result)
  }
}

def notifyHipchat (String status = 'STARTED') {

  // status of null means successful
  status = status ?: 'FINISHED'

  // default values
  def color = 'RED'
  def message = "Failed <strong>${env.JOB_NAME}</strong> build <strong>#${env.BUILD_NUMBER}</strong> - <a href=\"${env.BUILD_URL}console\">view console</a>"

  // override default values based on build status
  if (status == 'STARTED') {
    color = 'YELLOW'
    message = "Started <strong>${env.JOB_NAME}</strong> build <strong>#${env.BUILD_NUMBER}</strong> - <a href=\"${env.BUILD_URL}console\">view console</a>"
  }
  if (status == 'FINISHED') {
    color = 'GREEN'
    message = "Finished <strong>${env.JOB_NAME}</strong> build <strong>#${env.BUILD_NUMBER}</strong> - <a href=\"${env.BUILD_URL}changes\">view changes</a>"
  }

  // send notification
  hipchatSend(color: color, message: message)

}
